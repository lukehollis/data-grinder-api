# todo: use https://github.com/tiangolo/uwsgi-nginx-flask-docker for better performance
FROM python:3.6

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .
COPY config-template.py ./config.py
RUN ls -al

# env vars - from gcp config - need to be set in gitlab-ci:containerize
ARG TMP_DIR
ARG MICROSOFT_CS_KEY
ARG IMAGGA_SECRET
ARG IMAGGA_KEY
ARG CLARIFAI_API_KEY
ARG VISION_CRED
ARG AWS_ACCESS_KEY
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_REGION

RUN export

CMD [ "python", "./main.py" ]