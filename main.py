# Flask libs
from flask import Flask, jsonify
from flask import request
from flask import abort

# data grinder libs
import json
import argparse
import datetime
import config
import requests
from PIL import Image
from parsers import vision, imagga, iiif, mcsvision, colors, aws

# utils
from concurrent.futures import ThreadPoolExecutor
import threading
import time

# init the API
app = Flask(__name__)


@app.route('/')
def index():
    return "ok"

# get json format of results


@app.route('/json', methods=['POST'])
def get_json():
    # input checks
    if not request.json or not 'url' in request.json:
        abort(400)

    # url and by default all ML services
    args = {
        'url': request.json['url'],
        'services': request.json.get('services', []),
    }

    services = args['services']
    if len(services) < 1:
        services = ['imagga', 'gv', 'mcs', 'clarifai', 'color', 'aws']

    # process img
    results = main(args['url'], services)

    # done and respond
    res = {
        'args': args,
        'results': results,
    }
    return jsonify(res), 200


# data-grinder functions modified from this point
def main(url, services):
    image_info = process_image(url, services)
    return image_info


## HELPER FUNCTIONS ##
def get_image_id(URL):
    if not 'iiif' in URL:
        return ('ok', URL)
    r = requests.get(URL)
    if r.status_code == 200:
        status = "ok"
        # resolve arbitrary iiif url instead of fixed MIT ones
        id = r.url.split("/")[-2]

    else:
        status = "bad"
        id = ""

    return (status, id)


def download_image(URL, filename='temp.jpg'):
    r = requests.get(URL, timeout=5)
    if r.status_code == 200:
        status = "ok"
        # todo: use a better temp approach
        path = f'{config.TEMPORARY_FILE_DIR}/{filename}'

        with open(path, 'wb') as out:
            for chunk in r.iter_content(chunk_size=128):
                out.write(chunk)
        print(f"[INFO] Image file saved ... {path}:{status}")
    else:
        status = "bad"
        path = ""
        print(f"[ERROR] Downloading image file failed ... {r}")

    return (status, path)


def fetch_color_result(image):
      # Run through HAM color service
    print("Fetch color started")
    result = colors.Colors().fetch_colors(image['image_url'])
    image["colors"] = result["colors"]
    print("Task color finished {}".format(threading.current_thread()))


def fetch_clarifai_result(image):
    # Run through Clarifai
    print("Fetch clarifai started")
    result = clarifai.Clarifai().fetch(image['image_url'], image["idsid"])
    # Process concepts
    if result["outputs"] and "data" in result["outputs"][0]:
        for concept in result["outputs"][0]["data"]["concepts"]:
            concept["annotationFragment"] = image['annotationFragmentFullImage']
    image["clarifai"] = result
    print("Task clarifai finished {}".format(threading.current_thread()))


def fetch_mcs_result_description(image):
    # Run through Microsoft Cognitive Services
    print("Fetch mcs description started")
    if "microsoftvision" not in image:
        image["microsoftvision"] = {}
    result = mcsvision.MCSVision().fetch_description(image['image_local_path'])
    # Process description->captions
    if "description" in result:
        for caption in result["description"]["captions"]:
            caption["annotationFragment"] = image['annotationFragmentFullImage']
    image["microsoftvision"]["describe"] = result
    print("Task mcs_description finished {}".format(threading.current_thread()))


def fetch_mcs_result_analyze(image):
    print("Fetch mcs analyze started")
    if "microsoftvision" not in image:
        image["microsoftvision"] = {}
    result = mcsvision.MCSVision().fetch_analyze(image['image_local_path'])
    # Process description->captions
    if "description" in result:
        for caption in result["description"]["captions"]:
            caption["annotationFragment"] = image['annotationFragmentFullImage']
    # Process categories
    if "categories" in result:
        for category in result["categories"]:
            category["annotationFragment"] = image['annotationFragmentFullImage']
    # Process tags
    if "tags" in result:
        for tag in result["tags"]:
            tag["annotationFragment"] = image['annotationFragmentFullImage']
    # convert faces to IIIF image API URLs
    if "faces" in result:
        for index in range(len(result["faces"])):
            face = result["faces"][index]
            xOffset = face["faceRectangle"]["left"]*image["imageScaleFactor"]
            yOffset = face["faceRectangle"]["top"]*image["imageScaleFactor"]
            width = face["faceRectangle"]["width"]*image["imageScaleFactor"]
            height = face["faceRectangle"]["height"]*image["imageScaleFactor"]
            iiifFaceImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
            face["iiifFaceImageURL"] = iiifFaceImageURL
            face["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
            result["faces"][index] = face
    image["microsoftvision"]["analyze"] = result
    print("Task mcs_analyze finished {}".format(threading.current_thread()))


def fetch_gv_result(image):
    print("Fetch gc started")
    # Run through Google Vision
    result = vision.Vision().fetch(image['image_local_path'])
    print("Fetched gc result, processing...")
    # Process labels/tags
    if "labelAnnotations" in result["responses"][0]:
        for label in result["responses"][0]["labelAnnotations"]:
            label["annotationFragment"] = image['annotationFragmentFullImage']
    # convert bounding polys for face annotations to IIIF image API URL to fetch the specific region
    if "faceAnnotations" in result["responses"][0]:
        for index in range(len(result["responses"][0]["faceAnnotations"])):
            face = result["responses"][0]["faceAnnotations"][index]
            bounding = face["boundingPoly"]["vertices"]
            # sometimes the boundingPoly is missing X or Y values if it's too close to the edge of the image
            # fill in the missing coordinate using fdBoundingPoly
            fdBounding = face["fdBoundingPoly"]["vertices"]
            if "x" not in fdBounding[0]:
                fdBounding[0]["x"] = 0
            if "y" not in fdBounding[0]:
                fdBounding[0]["y"] = 0
            if "x" not in fdBounding[1]:
                fdBounding[1]["x"] = image["width"]
            if "y" not in fdBounding[1]:
                fdBounding[1]["y"] = 0
            if "x" not in fdBounding[2]:
                fdBounding[2]["x"] = image["width"]
            if "y" not in fdBounding[2]:
                fdBounding[2]["y"] = image["height"]
            if "x" not in fdBounding[3]:
                fdBounding[3]["x"] = 0
            if "y" not in fdBounding[3]:
                fdBounding[3]["y"] = image["height"]
            for i in range(len(bounding)):
                if "x" not in bounding[i]:
                    bounding[i]["x"] = fdBounding[i]["x"]
                if "y" not in bounding[i]:
                    bounding[i]["y"] = fdBounding[i]["y"]
            xOffset = bounding[0]["x"]*image["imageScaleFactor"]
            yOffset = bounding[0]["y"]*image["imageScaleFactor"]
            width = (bounding[1]["x"] - bounding[0]["x"]) * \
            image["imageScaleFactor"]
            height = (bounding[2]["y"] - bounding[0]["y"]) * \
            image["imageScaleFactor"]
            iiifFaceImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
            face["iiifFaceImageURL"] = iiifFaceImageURL
            face["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
            result["responses"][0]["faceAnnotations"][index] = face
    # convert bounding polys for text annotations to IIIF image API URL to fetch the specific region
    if "textAnnotations" in result["responses"][0]:
        regionPadding = 5
        for index in range(len(result["responses"][0]["textAnnotations"])):
            text = result["responses"][0]["textAnnotations"][index]
            bounding = text["boundingPoly"]["vertices"]
            # sometimes the boundingPoly is missing X or Y values if it's too close to the edge of the image
            # fill in the missing coordinate using the bounds of the actual image
            if "x" not in bounding[0]:
                bounding[0]["x"] = 0
            if "y" not in bounding[0]:
                bounding[0]["y"] = 0
            if "x" not in bounding[1]:
                bounding[1]["x"] = image["width"]
            if "y" not in bounding[1]:
                bounding[1]["y"] = 0
            if "x" not in bounding[2]:
                bounding[2]["x"] = image["width"]
            if "y" not in bounding[2]:
                bounding[2]["y"] = image["height"]
            if "x" not in bounding[3]:
                bounding[3]["x"] = 0
            if "y" not in bounding[3]:
                bounding[3]["y"] = image["height"]
            seqX = [x["x"] for x in bounding]
            seqY = [x["y"] for x in bounding]
            xOffset = min(seqX)*image["imageScaleFactor"]
            yOffset = min(seqY)*image["imageScaleFactor"]
            width = (max(seqX) - min(seqX)) * \
            image["imageScaleFactor"]+regionPadding
            height = (max(seqY) - min(seqY)) * \
            image["imageScaleFactor"]+regionPadding
            iiifTextImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
            text["iiifTextImageURL"] = iiifTextImageURL
            text["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
            result["responses"][0]["textAnnotations"][index] = text
    image["googlevision"] = result
    print("Task gv finished {}".format(threading.current_thread()))


def fetch_gv_text_result(image):
    print("Fetch gc started")
    # Run through Google Vision
    result = vision.Vision().fetch_text(image['image_local_path_full'])
    print("Fetched gc result, processing...")
    # Process labels/tags
    if "labelAnnotations" in result["responses"][0]:
        for label in result["responses"][0]["labelAnnotations"]:
            label["annotationFragment"] = image['annotationFragmentFullImage']
    # convert bounding polys for face annotations to IIIF image API URL to fetch the specific region
    if "faceAnnotations" in result["responses"][0]:
        for index in range(len(result["responses"][0]["faceAnnotations"])):
            face = result["responses"][0]["faceAnnotations"][index]
            bounding = face["boundingPoly"]["vertices"]
            # sometimes the boundingPoly is missing X or Y values if it's too close to the edge of the image
            # fill in the missing coordinate using fdBoundingPoly
            fdBounding = face["fdBoundingPoly"]["vertices"]
            if "x" not in fdBounding[0]:
                fdBounding[0]["x"] = 0
            if "y" not in fdBounding[0]:
                fdBounding[0]["y"] = 0
            if "x" not in fdBounding[1]:
                fdBounding[1]["x"] = image["width"]
            if "y" not in fdBounding[1]:
                fdBounding[1]["y"] = 0
            if "x" not in fdBounding[2]:
                fdBounding[2]["x"] = image["width"]
            if "y" not in fdBounding[2]:
                fdBounding[2]["y"] = image["height"]
            if "x" not in fdBounding[3]:
                fdBounding[3]["x"] = 0
            if "y" not in fdBounding[3]:
                fdBounding[3]["y"] = image["height"]
            for i in range(len(bounding)):
                if "x" not in bounding[i]:
                    bounding[i]["x"] = fdBounding[i]["x"]
                if "y" not in bounding[i]:
                    bounding[i]["y"] = fdBounding[i]["y"]
            xOffset = bounding[0]["x"]*image["imageScaleFactor"]
            yOffset = bounding[0]["y"]*image["imageScaleFactor"]
            width = (bounding[1]["x"] - bounding[0]["x"]) * \
            image["imageScaleFactor"]
            height = (bounding[2]["y"] - bounding[0]["y"]) * \
            image["imageScaleFactor"]
            iiifFaceImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
            face["iiifFaceImageURL"] = iiifFaceImageURL
            face["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
            result["responses"][0]["faceAnnotations"][index] = face
    # convert bounding polys for text annotations to IIIF image API URL to fetch the specific region
    if "textAnnotations" in result["responses"][0]:
        regionPadding = 5
        for index in range(len(result["responses"][0]["textAnnotations"])):
            text = result["responses"][0]["textAnnotations"][index]
            bounding = text["boundingPoly"]["vertices"]
            # sometimes the boundingPoly is missing X or Y values if it's too close to the edge of the image
            # fill in the missing coordinate using the bounds of the actual image
            if "x" not in bounding[0]:
                bounding[0]["x"] = 0
            if "y" not in bounding[0]:
                bounding[0]["y"] = 0
            if "x" not in bounding[1]:
                bounding[1]["x"] = image["width"]
            if "y" not in bounding[1]:
                bounding[1]["y"] = 0
            if "x" not in bounding[2]:
                bounding[2]["x"] = image["width"]
            if "y" not in bounding[2]:
                bounding[2]["y"] = image["height"]
            if "x" not in bounding[3]:
                bounding[3]["x"] = 0
            if "y" not in bounding[3]:
                bounding[3]["y"] = image["height"]
            seqX = [x["x"] for x in bounding]
            seqY = [x["y"] for x in bounding]
            xOffset = min(seqX)*image["imageScaleFactor"]
            yOffset = min(seqY)*image["imageScaleFactor"]
            width = (max(seqX) - min(seqX)) * \
            image["imageScaleFactor"]+regionPadding
            height = (max(seqY) - min(seqY)) * \
            image["imageScaleFactor"]+regionPadding
            iiifTextImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
            text["iiifTextImageURL"] = iiifTextImageURL
            text["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
            result["responses"][0]["textAnnotations"][index] = text
    image["googlevision"] = result
    print("Task gv finished {}".format(threading.current_thread()))


def fetch_imagga_result_tags(image):
    print("Fetch imagga_tags started")
    # Run through Imagga
    if "imagga" not in image:
        image["imagga"] = {}
    # Process tags
    result = imagga.Imagga().fetch(image['image_url'])
    print("Fetched imagga_tags")
    if "result" in result and "tags" in result["result"]:
        for tag in result["result"]["tags"]:
            tag["annotationFragment"] = image['annotationFragmentFullImage']
    image["imagga"]["tags"] = result
    print("Task imagga_tags finished {}".format(threading.current_thread()))


def fetch_imagga_result_categories(image):
    print("Fetch imagga_categories started")
    if "imagga" not in image:
        image["imagga"] = {}
    # Process categories
    result = imagga.Imagga().fetch_categories(image['image_url'])
    print("Fetched imagga_categories")
    if "result" in result and "categories" in result["result"]:
        for category in result["result"]["categories"]:
            category["annotationFragment"] = image['annotationFragmentFullImage']
    image["imagga"]["categories"] = result
    print("Task imagga_categories finished {}".format(threading.current_thread()))


def fetch_aws_result_labels(image):
    print("Fetch aws_labels started")
    # Run through AWS Rekognition
    if "aws" not in image:
        image["aws"] = {}
    # Process labels
    result = aws.AWS().fetch_labels(image['image_local_path'])
    if "Labels" in result:
        for label in result["Labels"]:
            label["annotationFragment"] = image['annotationFragmentFullImage']
            for instance in label["Instances"]:
                if "BoundingBox" in instance:
                    xOffset = (image["width"]*instance["BoundingBox"]
                               ["Left"])*image["imageScaleFactor"]
                    yOffset = (image["height"]*instance["BoundingBox"]
                               ["Top"])*image["imageScaleFactor"]
                    width = (image["width"]*instance["BoundingBox"]
                             ["Width"])*image["imageScaleFactor"]
                    height = (image["height"]*instance["BoundingBox"]
                              ["Height"])*image["imageScaleFactor"]
                    iiifImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                        yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
                    instance["iiifLabelImageURL"] = iiifImageURL
                    instance["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                        int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
    image["aws"]["labels"] = result
    print("Task aws_labels finished {}".format(threading.current_thread()))


def fetch_aws_result_face(image):
    print("Fetch aws_face started")
    if "aws" not in image:
        image["aws"] = {}
    # Process faces
    result = aws.AWS().fetch_faces(image['image_local_path'])
    if "FaceDetails" in result:
        for face in result["FaceDetails"]:
            if "BoundingBox" in face:
                xOffset = (image["width"]*face["BoundingBox"]
                           ["Left"])*image["imageScaleFactor"]
                yOffset = (image["height"]*face["BoundingBox"]
                           ["Top"])*image["imageScaleFactor"]
                width = (image["width"]*face["BoundingBox"]
                         ["Width"])*image["imageScaleFactor"]
                height = (image["height"]*face["BoundingBox"]
                          ["Height"])*image["imageScaleFactor"]
                iiifImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                    yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
                face["iiifFaceImageURL"] = iiifImageURL
                face["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                    int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
    image["aws"]["faces"] = result
    print("Task aws_faces finished {}".format(threading.current_thread()))


def fetch_aws_result_text(image):
    print("Fetch aws_text started")
    if "aws" not in image:
        image["aws"] = {}
    # Process text
    result = aws.AWS().fetch_text(image['image_local_path'])
    if "TextDetections" in result:
        for text in result["TextDetections"]:
            if "Geometry" in text:
                boundingBox = text["Geometry"]["BoundingBox"]
                xOffset = (image["width"]*boundingBox["Left"]) * \
                image["imageScaleFactor"]
                yOffset = (image["height"]*boundingBox["Top"]) * \
                image["imageScaleFactor"]
                # Sometimes width and height are reported as negative values. AWS documentation doesn't say why this happens.
                # I'm using ABS as a hack to make values that work in IIIF fragments
                width = (image["width"]*abs(boundingBox["Width"])
                         )*image["imageScaleFactor"]
                height = (image["height"]*abs(boundingBox["Height"])
                          )*image["imageScaleFactor"]
                iiifImageURL = image["iiifbaseuri"] + "/" + str(int(xOffset)) + "," + str(int(
                    yOffset)) + "," + str(int(width)) + "," + str(int(height)) + "/full/0/default.jpg"
                text["iiifTextImageURL"] = iiifImageURL
                text["annotationFragment"] = "xywh=" + str(int(xOffset)) + "," + str(
                    int(yOffset)) + "," + str(int(width)) + "," + str(int(height))
    image["aws"]["text"] = result
    print("Task aws_text finished {}".format(threading.current_thread()))


def fetch_results_from_APIs_concurrent(image):
    """concurrently calls multiple AI APIs to fetch results and return modified image object with results

    Arguments:
            image {[type]} -- [description]
    """
    print("Starting ThreadPoolExecutor")
    futures = {}
    with ThreadPoolExecutor(max_workers=10) as executor:
        if "color" in image['services']:
            futures["color"] = executor.submit(fetch_color_result, (image))
        if "clarifai" in image['services']:
            futures["clarifai"] = executor.submit(
                fetch_clarifai_result, (image))
        if "mcs" in image['services']:
            futures["mcs_description"] = executor.submit(
                fetch_mcs_result_description, (image))
            futures["mcs_analyze"] = executor.submit(
                fetch_mcs_result_analyze, (image))
        if "gv" in image['services']:
            futures["gv"] = executor.submit(fetch_gv_result, (image))
        if "gv_text" in image['services']:
            futures["gv_text"] = executor.submit(fetch_gv_text_result, (image))
        if "imagga" in image['services']:
            futures["image_tags"] = executor.submit(
                fetch_imagga_result_tags, (image))
            futures["image_categories"] = executor.submit(
                fetch_imagga_result_categories, (image))
        if "aws" in image['services']:
            futures["aws_label"] = executor.submit(
                fetch_aws_result_labels, (image))
            futures["aws_face"] = executor.submit(
                fetch_aws_result_face, (image))
            futures["aws_text"] = executor.submit(
                fetch_aws_result_text, (image))

    print("All tasks complete ... status: ", futures)
    return futures


def process_image(URL, services):
    """prepare metadata from the image and call concurrent functions to fetch AI results from multiple APIs

    Arguments:
            URL {[type]} -- [description]
            services {[type]} -- [description]

    Returns:
            [type] -- [description]
    """
    print(f"[INFO] Received request for processing image ... {URL}")
    start_time = time.time()
    image = {
        "url": URL,
        "lastupdated": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "services": services,
    }

    # get IDS ID
    (status, id) = get_image_id(URL)

    image["drsstatus"] = status

    if status == "ok":
        # generate iiif image url
        image_url = iiif.IIIFImage.get_general_image_url(id)
        image["image_url"] = image_url

        image_url_full = iiif.IIIFImage.get_full_image_url(id)
        image["image_url_full"] = image_url_full

        image["idsid"] = id
        image["iiifbaseuri"] = iiif.IIIFImage.get_base_uri(id)

        # Download the image
        (status, image_local_path) = download_image(image_url)
        image["image_local_path"] = image_local_path

        # Download full image for text detection
        if 'gv_text' in services:
            (_, image_local_path_full) = download_image(
                image_url_full, 'temp_full.jpg')
            image["image_local_path_full"] = image_local_path_full


        # Gather and store image metadata
        im = Image.open(image_local_path)
        size = im.size  # (width,height) tuple
        image["width"] = size[0]
        image["height"] = size[1]


        iiif_image_info = iiif.IIIFImage().fetch(image["iiifbaseuri"])
        image["widthFull"] = iiif_image_info["width"]
        image["heightFull"] = iiif_image_info["height"]

        imageScaleFactor = iiif_image_info["width"]/image["width"]
        image["imageScaleFactor"] = imageScaleFactor

        annotationFragmentFullImage = "xywh=0,0," + \
            str(int(image["width"])) + "," + str(int(image["height"]))
        image["annotationFragmentFullImage"] = annotationFragmentFullImage

        # scalefactor is useful when converting annotation coordinates between different image sizes
        image["scalefactor"] = imageScaleFactor

        # concurrently fetch results from APIs
        futures = fetch_results_from_APIs_concurrent(image)

        image["futures"] = list(map(lambda x: str(x), futures.items()))

        

    time_diff = time.time() - start_time
    image['queryTime'] = time_diff
    print(f"Finished processing {URL} in {time_diff} seconds")
    return image


def application(environ, start_response):
    return app.wsgi_app(environ, start_response)


# [START run_application]
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)

    # CLI disabled from this point on
    #parser = argparse.ArgumentParser()
    #parser.add_argument('-url', nargs='?', default=None, required=True)
    #parser.add_argument('-services', nargs='+', choices=['imagga', 'gv', 'mcs', 'clarifai', 'color', 'aws'], default=['imagga', 'gv', 'mcs', 'clarifai', 'color', 'aws'])
    #args = parser.parse_args()

    #main(args.url, args.services)

# [END run_application]
