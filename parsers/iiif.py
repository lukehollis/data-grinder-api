import requests


class IIIFImage(object):

    def get_base_uri(id):
        if 'http' in id:
            return id
        return 'http://iiif.orphe.us/%s' % id

    def get_general_image_url(id):
        if 'http' in id:
            return id
        return 'http://iiif.orphe.us/%s/full/512,/0/default.jpg' % id

    def get_full_image_url(id):
        if 'http' in id:
            return id
        return 'http://iiif.orphe.us/%s/full/full/0/default.jpg' % id

    def fetch(self, iiifImageURI):
        response = requests.get('%s/info.json' % iiifImageURI, timeout=30)
        r_json = {
            'width': 0,
            'height': 0
        }
        try:
            r_json = response.json()
        except:
            return r_json
        return r_json
